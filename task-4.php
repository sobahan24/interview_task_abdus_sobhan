<?php

class Animal {
    public $name;

    public function __construct($name) {
        $this->name = $name;
    }

    public function makeSound() {
        return "Some generic sound";
    }
}

class Dog extends Animal {
    public function makeSound() {
        return "Woof!";
    }
}

class Cat extends Animal {
    public function makeSound() {
        return "Meow!";
    }
}

class Cow extends Animal {
    public function makeSound() {
        return "Moo!";
    }
}

// Create instances of animals
$dog = new Dog("Buddy");
$cat = new Cat("Whiskers");
$cow = new Cow("Bessie");

// Call the makeSound method on each animal
$animals = [$dog, $cat, $cow];
foreach ($animals as $animal) {
    echo $animal->name . ": " . $animal->makeSound() . "<br>";
}


?>