
@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Session::has('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('status') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
        @endif
        <h1>To-do List</h1>

        <a href="{{ route('todos.create') }}" class="btn btn-primary mb-3">Create New Task</a>

        <ul class="list-group">
            @foreach($todos as $todo)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    {{ $todo->title }}
                    <span>
                        <a href="{{ route('todos.edit', $todo) }}" class="btn btn-sm btn-info">Edit</a>
                        <form action="{{ route('todos.destroy', $todo) }}" method="POST" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>
                    </span>
                </li>
            @endforeach
        </ul>
    </div>
@endsection
