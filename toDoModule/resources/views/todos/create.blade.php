
@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Create New Task</h1>

        <form action="{{ route('todos.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control">
            </div>
            <button type="submit" class="btn btn-success mt-2">Create</button>
        </form>
    </div>
@endsection
