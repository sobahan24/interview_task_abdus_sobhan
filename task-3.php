<?php


class Employee {
    private $name;
    private $id;
    private $salary;

    public function __construct($name, $id, $salary) {
        $this->name = $name;
        $this->id = $id;
        $this->setSalary($salary);
    }

    // Getter for name
    public function getName() {
        return $this->name;
    }

    // Getter for employee ID
    public function getID() {
        return $this->id;
    }

    // Getter for salary
    public function getSalary() {
        return $this->salary;
    }

    // Setter for salary with encapsulation
    public function setSalary($newSalary) {
        if ($newSalary > 0) {
            $this->salary = $newSalary;
        } else {
            throw new InvalidArgumentException("Salary must be greater than 0");
        }
    }
}

// Creating an employee object
try {
    $employee = new Employee("John", 123, 50000);
    echo "Employee Name: " . $employee->getName() . "<br>";
    echo "Employee ID: " . $employee->getID() . "<br>";
    echo "Employee Salary: $" . $employee->getSalary() . "<br>";

    // Trying to set an invalid salary
    $employee->setSalary(-1000);
} catch (InvalidArgumentException $e) {
    echo "Error: " . $e->getMessage();
}


?>