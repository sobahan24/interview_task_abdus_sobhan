<?php


class ParentClass {
    public $property1;

    public function method1() {
        echo "Method 1 from ParentClass";
    }
}

class ChildClass extends ParentClass {
    public $property2;

    public function method2() {
        echo "Method 2 from ChildClass";
    }
}


$childObj = new ChildClass();
$childObj->property1 = "Value for property1"; // Property from ParentClass
$childObj->property2 = "Value for property2"; // Property from ChildClass

$childObj->method1(); // Method from ParentClass
$childObj->method2();




?>


